﻿using backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Interfaces
{
    public interface IHistoricoItemRepository
    {
        Task<bool> SaveAllAsync();

        void Add(HistoricoItem item);

        Task<IEnumerable<HistoricoItem>> GetHistoricoItemsAsync(int pageNumber, int size,  string colabFilter, string clientFilter);
        Task<int> GetTotalRecords();
        Task<HistoricoItem> GetHistoricoItemByIdAsync(int id);
        Task<HistoricoItem> GetHistoricoItemByNameAsync(string name);
    }
}
