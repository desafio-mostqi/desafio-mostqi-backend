﻿using backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Interfaces
{
    public interface ICotacaoRepository
    {

        Task<IEnumerable<Cotacao>> GetCotacaoAsync(int pageNumber, int size, DateTime? filterBegin, DateTime? filterEnd);

        Task Add(Cotacao cotacao);

        Task<int> GetTotalRecords();
    }
}
