using System.Collections.Generic;
using System.Threading.Tasks;
using backend.Entities;

namespace backend.Interfaces
{
    public interface IColaboradorRepository
    {
         void Update(Colaborador colaborador);

         Task<bool> SaveAllAsync();

         Task<IEnumerable<Colaborador>> GetColaboradoresAsync(int pageNumber, int size, string filter);

         Task<int> GetTotalRecords();

         Task<Colaborador> GetColaboradorByIdAsync(int id);

         Task<Colaborador> GetColaboradorByNameAsync(string name);
    }
}