using System.Threading.Tasks;
using backend.DTOs;

namespace backend.Interfaces
{
    public interface IMostQiService
    {
        
        Task<string> Authenticate();
        Task<ContentExtracted> ExtractContent(ContentToBeExtracted content);

        Task<ContentExtracted> FaceExtractFeatures(ContentToBeExtracted content);
    }
}