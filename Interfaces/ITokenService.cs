using backend.Entities;

namespace backend.Interfaces
{
    public interface ITokenService
    {
         string CreateColaboradorToken(Colaborador user);
         string CreateClienteToken(Cliente user);

    }
}