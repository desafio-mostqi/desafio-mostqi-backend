using System.ComponentModel.DataAnnotations;

namespace backend.DTOs
{
    public interface IUserDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Token { get; set; }

        public bool isColaborador { get; set; }
    }
}