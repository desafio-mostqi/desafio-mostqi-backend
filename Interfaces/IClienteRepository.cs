using System.Collections.Generic;
using System.Threading.Tasks;
using backend.Entities;

namespace backend.Interfaces
{
    public interface IClienteRepository
    {
        Task Update(Cliente Cliente);

         Task<bool> SaveAllAsync();

         Task<IEnumerable<Cliente>> GetClientesAsync(int pageNumber, int size, string filter);

         Task<int> GetTotalRecords();

         Task<Cliente> GetClienteByIdAsync(int id);

         Task<Cliente> GetClienteByNameAsync(string name);
    }
}