﻿
using System.Drawing;
using System.Drawing.Imaging;


namespace backend.Interfaces
{
    public interface IImageService
    {

        string ImageToBase64(Image image, ImageFormat format);
        
        Image Base64ToImage(string base64String);

        Bitmap CropImage(Image source, int x, int y, int width, int height);
    }
}
