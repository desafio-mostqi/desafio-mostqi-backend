﻿using backend.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace backend.Background
{
    public class ConsumeScoppedDolarService : BackgroundService
    {
        private readonly IServiceProvider _services;
        public ConsumeScoppedDolarService(IServiceProvider services)
        {
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(1000 * 60, stoppingToken);

                using (var scope = _services.CreateScope())
                {
                    var dolarService =
                        scope.ServiceProvider
                            .GetRequiredService<IDolarService>();

                    await dolarService.GetCotacao(stoppingToken);
                }
            }
        }
    }
}
