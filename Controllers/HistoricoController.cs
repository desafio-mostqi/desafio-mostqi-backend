﻿using AutoMapper;
using backend.DTOs;
using backend.Entities;
using backend.Helpers;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    public class HistoricoController : BaseApiController
    {

        private readonly IHistoricoItemRepository _historicoItemRepository;

        private readonly IMapper _mapper;
        public HistoricoController(IHistoricoItemRepository historicoItemRepository, IMapper mapper)
        {
            _historicoItemRepository = historicoItemRepository;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HistoricoItem>>> GetHistorico([FromQuery] int pageNumber, int pageSize, string colab, string client)
        {
            var validFilter = new PaginationFilter(colab, client ,pageNumber, pageSize);

            var result = await _historicoItemRepository.GetHistoricoItemsAsync(validFilter.PageNumber, validFilter.PageSize, validFilter.ColabFilter, validFilter.ClientFilter);

            var items = _mapper.Map<IEnumerable<HistoricoItemDto>>(result);

            var records = await _historicoItemRepository.GetTotalRecords();

            return Ok(new PagedResponse<IEnumerable<HistoricoItemDto>>(items, validFilter.PageNumber, validFilter.PageSize, records));

        }


    }
}
