using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Data;
using backend.DTOs;
using backend.Entities;
using backend.Helpers;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace backend.Controllers
{

    public class ColaboradorController : BaseApiController
    {
        private readonly IColaboradorRepository _colaboradorRepository;
        public ColaboradorController(IColaboradorRepository colaboradorRepository)
        {
            _colaboradorRepository = colaboradorRepository;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Colaborador>>> GetColaboradors([FromQuery] int pageNumber, int pageSize, string colab)
        {
            var validFilter = new PaginationFilter(colab, pageNumber, pageSize);

            var result = await _colaboradorRepository.GetColaboradoresAsync(validFilter.PageNumber, validFilter.PageSize, validFilter.ClientFilter);
            var records = await _colaboradorRepository.GetTotalRecords();

            Ok(new PagedResponse<IEnumerable<Colaborador>>(result, validFilter.PageNumber, validFilter.PageSize, records));
            return Ok();
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<Colaborador>> GetUser(int id)
        {
            var result = await _colaboradorRepository.GetColaboradorByIdAsync(id);
            return Ok(result); ;

        }
    }
}