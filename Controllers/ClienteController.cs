using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using backend.Data;
using backend.DTOs;
using backend.Entities;
using backend.Helpers;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace backend.Controllers
{

    public class ClienteController : BaseApiController
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly IColaboradorRepository _colaboradorRepository;
        private readonly IHistoricoItemRepository _historicoItemRepository;
        private readonly IMapper _mapper;
        public ClienteController(IClienteRepository clienteRepository, IHistoricoItemRepository historicoItemRepository, IColaboradorRepository colaboradorRepo, IMapper mapper)
        {
            _clienteRepository = clienteRepository;
            _colaboradorRepository = colaboradorRepo;
            _historicoItemRepository = historicoItemRepository;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cliente>>> GetClientes([FromQuery] int pageNumber, int pageSize, string cliente)
        {
            var validFilter = new PaginationFilter(cliente, pageNumber, pageSize);

            var result = await _clienteRepository.GetClientesAsync(validFilter.PageNumber, validFilter.PageSize, validFilter.ClientFilter);
            var records = await _clienteRepository.GetTotalRecords();

            return Ok(new PagedResponse<IEnumerable<Cliente>>(result, validFilter.PageNumber, validFilter.PageSize, records));            

        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ClienteDto>> GetClienteById(int id)
        {
            var item = await _clienteRepository.GetClienteByIdAsync(id);
            var result = _mapper.Map<ClienteDto>(item);
            return Ok(result);

        }


        [HttpPut, Authorize]
        public async Task<ActionResult<ClienteDto>> UpdateCliente([FromBody] ClienteDto cliente)
        {
            try
            {
                var user = await _colaboradorRepository.GetColaboradorByIdAsync(cliente.CurrentUser);

                if (user == null) return Unauthorized("Unauthorized user");

                using var hmac = new HMACSHA512();

                var userFromDb = await _clienteRepository.GetClienteByIdAsync(cliente.Id);

                if(userFromDb.UserName != cliente.Username)
                {
                    var clientesCont = await _clienteRepository.GetClienteByNameAsync(cliente.Username);
                    var colabCont = await _colaboradorRepository.GetColaboradorByNameAsync(cliente.Username);

                    if(clientesCont != null  || colabCont != null) return BadRequest("Invalid Email.");
                }

                var item = new Cliente
                {
                    Id = cliente.Id,
                    UserName = cliente.Username.ToLower(),
                    ImageBase64 = cliente.ImageBase64,
                    DataNascimento = cliente.DataNascimento,
                    Rg = cliente.Rg,
                    Name = cliente.Name
                };

                _clienteRepository.Update(item);

                var c = await _clienteRepository.GetClienteByIdAsync(cliente.Id);

                var registro = new HistoricoItem
                {
                    Acao = "atualizacao cliente",
                    Cliente = c,
                    Colaborador = user,
                    Data = DateTime.Now
                };

                _historicoItemRepository.Add(registro);

                return Ok(item);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }


        }
    }
}