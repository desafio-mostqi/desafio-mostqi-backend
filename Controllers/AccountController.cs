using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using backend.Data;
using backend.DTOs;
using backend.Entities;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly DataContext _context;
        private readonly ITokenService _tokenService;

        public AccountController(DataContext context, ITokenService tokenService)
        {
            _context = context;
            _tokenService = tokenService;
        }

        [HttpPost("register-colaborador")]
        public async Task<ActionResult<ColaboradorDto>> RegisterColaborador(ColaboradorDto registerDto)
        {
            if (await UserExists(registerDto.Username)) return BadRequest("User Alredy Exists");

            using var hmac = new HMACSHA512();

            var user = new Colaborador
            {
                UserName = registerDto.Username.ToLower(),
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key
            };

            _context.Colaboradores.Add(user);
            await _context.SaveChangesAsync();

            return new ColaboradorDto
            {
                Id = user.Id,
                Username = user.UserName,
                Token = _tokenService.CreateColaboradorToken(user)
            };
        }

        [HttpPost("register-cliente"), Authorize]
        public async Task<ActionResult<ClienteDto>> RegisterCliente(ClienteDto registerDto)
        {
            if (await UserExists(registerDto.Username)) return BadRequest("User Alredy Exists");

            var user = await _context.Colaboradores.FirstOrDefaultAsync(x => x.Id == registerDto.CurrentUser);

            if (user == null) return Unauthorized("Unauthorized user");

            using var hmac = new HMACSHA512();

            var client = new Cliente
            {
                UserName = registerDto.Username.ToLower(),
                DataNascimento = registerDto.DataNascimento,
                Rg = registerDto.Rg,
                Name = registerDto.Name,
                ImageBase64 = registerDto.ImageBase64,
                PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password)),
                PasswordSalt = hmac.Key
            };


            var registro = new HistoricoItem
            {
                Data = DateTime.Now,
                Acao = "registro cliente",
                Cliente = client,
                Colaborador = user
            };

            _context.Clientes.Add(client);

            _context.ItemsHistorico.Add(registro);
            await _context.SaveChangesAsync();

            return new ClienteDto
            {
                Id = client.Id,
                Username = client.UserName,
                Token = _tokenService.CreateClienteToken(client)
            };
        }

        [HttpPost("login")]
        public async Task<ActionResult<IUserDto>> LoginColaborador(LoginDto loginDto)
        {
            
            
            var user = await _context.Colaboradores.SingleOrDefaultAsync(x => x.UserName == loginDto.Username);

            if (user == null)
            {
                var client = await _context.Clientes.SingleOrDefaultAsync(x => x.UserName == loginDto.Username);
                if (client == null) return Unauthorized("Invalid Username");
                else
                {

                    using var hmac = new HMACSHA512(client.PasswordSalt);

                    var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

                    for (int i = 0; i < computedHash.Length; i++)
                    {
                        if (computedHash[i] != client.PasswordHash[i]) return Unauthorized("Incorrect Password");
                    }
                    return new ClienteDto
                    {
                        Id = client.Id,
                        Username = client.UserName,
                        Token = _tokenService.CreateClienteToken(client)
                    };
                }
            }
            else
            {
                using var hmac = new HMACSHA512(user.PasswordSalt);

                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != user.PasswordHash[i]) return Unauthorized("Incorrect Password");
                }
                return new ColaboradorDto
                {
                    Id = user.Id,
                    Username = user.UserName,
                    Token = _tokenService.CreateColaboradorToken(user)
                };
            }


        }




        private async Task<bool> UserExists(string username)
        {
            return await _context.Colaboradores.AnyAsync(x => x.UserName == username.ToLower()) || await _context.Clientes.AnyAsync(x => x.UserName == username.ToLower());
        }

    }
}