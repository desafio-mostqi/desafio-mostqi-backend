using System.Threading.Tasks;
using backend.DTOs;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    public class MostQiController : BaseApiController
    {
        private readonly IMostQiService _mostQiService;
        public MostQiController(IMostQiService mostQiService)
        {
            _mostQiService = mostQiService;
        }

        [HttpPost, Authorize]
        [Route("process-image")]
        public async Task<ActionResult<ContentExtracted>> ExtractContent([FromBody] ContentToBeExtracted content)
        {
            var result = await _mostQiService.ExtractContent(content);
            return Ok(result);
        }

        [HttpPost, Authorize]
        [Route("face-extract-features")]
        public async Task<ActionResult<ContentExtracted>> FaceExtractFeatures([FromBody] ContentToBeExtracted content)
        {

            var result = await _mostQiService.FaceExtractFeatures(content);
            return Ok(result);
        }
    }
}