﻿using AutoMapper;
using backend.DTOs;
using backend.Entities;
using backend.Helpers;
using backend.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    public class CotacaoController : BaseApiController
    {

        private readonly ICotacaoRepository _cotacaoRepository;
        private readonly IMapper _mapper;

        public CotacaoController(ICotacaoRepository cotacaoRepository)
        {
            _cotacaoRepository = cotacaoRepository;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cotacao>>> GetCotacao([FromQuery] int pageNumber, int pageSize, DateTime? filterBegin, DateTime? filterEnd)
        {
            var validFilter = new PaginationFilter(pageNumber, pageSize, filterBegin, filterEnd);
            var result = await _cotacaoRepository.GetCotacaoAsync(validFilter.PageNumber, validFilter.PageSize, validFilter.DataInicio, validFilter.DataFim);
            var records = await _cotacaoRepository.GetTotalRecords();

            return Ok(new PagedResponse<IEnumerable<Cotacao>>(result, validFilter.PageNumber, validFilter.PageSize, records));

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<IEnumerable<Cotacao>>> AddCotacao([FromBody] Cotacao cotacao)
        {
            await _cotacaoRepository.Add(cotacao);
            return Ok(cotacao);
        }
    }
}
