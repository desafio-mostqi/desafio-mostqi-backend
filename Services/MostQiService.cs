using backend.DTOs;
using backend.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace backend.Services
{
    public class MostQiService : IMostQiService
    {
        private readonly string _apiKey ;
        private readonly IImageService _imageService;
        private static readonly HttpClient _client = new HttpClient();
        private readonly string _baseUrl ;

        public MostQiService(IImageService imageService, IConfiguration config)
        {
            _imageService = imageService;
            _apiKey = config["MostApiKey"];
            _baseUrl = config["MostBaseUrl"];

        }

        public async Task<ContentExtracted> ExtractContent(ContentToBeExtracted content)
        {
            var jwt = await Authenticate();

            if (jwt == null) throw new Exception();

            var json = JsonConvert.SerializeObject(content);

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt);

            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(_baseUrl + "/process-image/content-extraction", httpContent);

            return JsonConvert.DeserializeObject<ContentExtracted>(await response.Content.ReadAsStringAsync());
        }


        public async Task<ContentExtracted> FaceExtractFeatures(ContentToBeExtracted content)
        {
            var jwt = await Authenticate();

            if (jwt == null) throw new Exception();

            var json = JsonConvert.SerializeObject(content);

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt);

            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(_baseUrl + "/process-image/biometrics/face-extract-features", httpContent);

            var data = JsonConvert.DeserializeObject<ContentExtracted>(await response.Content.ReadAsStringAsync());

            var image = _imageService.Base64ToImage(content.FileBase64);

            if (data.Result.Length > 0)
            {
                var bottomRight = data.Result[0].Vertices.FirstOrDefault(x => x.Name.Equals("bottom_right")) ;
                var topLeft = data.Result[0].Vertices.FirstOrDefault(x => x.Name.Equals("top_left")) ;
                
                try
                {
                    var cuttedImage = _imageService.CropImage(image, topLeft.X, topLeft.Y, bottomRight.X - topLeft.X, topLeft.Y - bottomRight.Y);
                    data.Result[0].Image = _imageService.ImageToBase64((Image)cuttedImage, ImageFormat.Jpeg);
                } catch
                {
                    data.Result[0].Image = "";
                }
                                
            }
            
            
            return data;


        }


        public async Task<string> Authenticate()
        {
            var values = new Dictionary<string, string>
            {
                { "token", _apiKey },
            };

            string json = JsonConvert.SerializeObject(values);

            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(_baseUrl + "/user/authenticate", httpContent);

            return JsonConvert.DeserializeObject<AuthDto>(await response.Content.ReadAsStringAsync()).Token;
        }


    }
}