﻿using backend.DTOs;
using backend.Entities;
using backend.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace backend.Services
{
    public class DolarService : IDolarService
    {

        private readonly string _apiKey ;
        private readonly ICotacaoRepository _cotacaoRepository;
        private static readonly HttpClient _client = new HttpClient();
        private readonly string _baseUrl ;

        public DolarService(ICotacaoRepository cotacaoRepository , IConfiguration config)
        {
            _cotacaoRepository = cotacaoRepository;
            _apiKey = config["DolarApiKey"];
            _baseUrl = config["DolarBaseUrl"];
        }

        public async Task GetCotacao(CancellationToken stoppingToken)
        {

            try
            {
                var response = await _client.GetAsync(_baseUrl + $"time_series?symbol=USD/BRL,&interval=1min&apikey={_apiKey}");

                var data = JsonConvert.DeserializeObject<CotacaoFromAPI>(await response.Content.ReadAsStringAsync());


                //(string valor, string high, string low, DateTime data)
                if (data.UsdBrl.Values.Length > 0)
                {
                    var date = data.UsdBrl.Values[0].Datetime.AddHours(-3);
                    var obj = new Cotacao(data.UsdBrl.Values[0].Close.ToString(), data.UsdBrl.Values[0].High.ToString(), data.UsdBrl.Values[0].Low.ToString(), date);
                    await _cotacaoRepository.Add(obj);
                }
            }
            catch
            {

            }


        }
    }
}
