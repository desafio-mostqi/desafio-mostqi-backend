﻿using backend.Interfaces;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace backend.Services
{
    public class ImageService : IImageService
    {

        private readonly IClienteRepository _clienteRepository;

        public ImageService(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }


        public Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }


        public string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to base 64 string
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public Bitmap CropImage(Image source, int x, int y, int width, int height)
        {
            Rectangle crop = new Rectangle(x, y, width, height + 80);

            //Bitmap nb = new Bitmap(crop.Width, crop.Height);
            //using (Graphics g = Graphics.FromImage(nb))
            //{
            //    g.DrawImage(source, -crop.X, -crop.Y);
            //    return nb;
            //}

            Bitmap bmpImage = new Bitmap(source);
            return bmpImage.Clone(crop, bmpImage.PixelFormat);
            //return bmpCrop;

            //Bitmap bmpImage = new Bitmap(source);
            //return bmpImage.Clone(crop, bmpImage.);

            //var bmp = new Bitmap(crop.Width, crop.Height);
            //using (var gr = Graphics.FromImage(bmp))
            //{
            //    gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            //}
            //return bmp;
        }

    }
}
