using backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace backend.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Cliente> Clientes { get; set; }

        public DbSet<Colaborador> Colaboradores { get; set; }

        public DbSet<HistoricoItem> ItemsHistorico { get; set; }

        public DbSet<Cotacao> Cotacoes { get; set; }

    }
}