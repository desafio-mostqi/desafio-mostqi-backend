﻿using backend.Entities;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Data.Repository
{
    public class HistoricoItemRepository : IHistoricoItemRepository
    {
        private readonly DataContext _context;


        public HistoricoItemRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<HistoricoItem> GetHistoricoItemByIdAsync(int id)
        {
            return await _context.ItemsHistorico.FindAsync(id);
        }

        public async Task<HistoricoItem> GetHistoricoItemByNameAsync(string name)
        {

            return await _context.ItemsHistorico.SingleOrDefaultAsync(x => x.Cliente.UserName == name);
        }

        public async Task<IEnumerable<HistoricoItem>> GetHistoricoItemsAsync(int pageNumber, int size, string colabFilter, string clientFilter)
        {

            return await _context.ItemsHistorico
                .Where(x => x.Colaborador.UserName.ToLower().Contains(colabFilter.ToLower()) && x.Cliente.UserName.ToLower().Contains(clientFilter.ToLower()))
                 .OrderByDescending(x => x.Data)
                .Include(x => x.Cliente)
                .Include(y => y.Colaborador)
                .Skip((pageNumber - 1) * size)
                .Take(size)
                .ToListAsync();
        }
        public async Task<int> GetTotalRecords()
        {
            return await _context.ItemsHistorico.CountAsync();
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(HistoricoItem historicoItem)
        {
            _context.Entry(historicoItem).State = EntityState.Modified;
        }

        public async void Add(HistoricoItem item)
        {
            _context.Add(item);
            await _context.SaveChangesAsync();
        }
    }
}
