﻿using backend.Entities;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Data.Repository
{
    public class CotacaoRepository : ICotacaoRepository
    {
        private readonly DataContext _context;

        public CotacaoRepository(DataContext context)
        {
            _context = context;
        }

        public async Task Add(Cotacao cotacao)
        {
            _context.Cotacoes.Add(cotacao);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Cotacao>> GetCotacaoAsync(int pageNumber, int size, DateTime? filterBegin, DateTime? filterEnd)
        {
            if (filterBegin != null && filterEnd != null)
                return await _context.Cotacoes
                   .Where(x => x.Data.CompareTo(filterBegin) >= 0 && x.Data.CompareTo(filterEnd) <= 0)
                   .OrderByDescending(x => x.Data)
                   .Skip((pageNumber - 1) * size)
                   .Take(size)
                   .ToListAsync();
            return await _context.Cotacoes
                   .OrderByDescending(x => x.Data)
                   .Skip((pageNumber - 1) * size)
                   .Take(size)
                   .ToListAsync();
        }

        public async Task<int> GetTotalRecords()
        {
            return await _context.Cotacoes.CountAsync();
        }
    }
}
