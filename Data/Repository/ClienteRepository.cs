using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Data.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly DataContext _context;


        public ClienteRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Cliente> GetClienteByIdAsync(int id)
        {
            return await _context.Clientes.FindAsync(id);
        }

        public async Task<Cliente> GetClienteByNameAsync(string name)
        {
            
            return await  _context.Clientes.SingleOrDefaultAsync(x => x.UserName == name);
        }

        public async Task<IEnumerable<Cliente>> GetClientesAsync(int pageNumber, int size, string filter)
        {
            return await  _context.Clientes
                .Where(x => x.UserName.ToLower().Contains(filter.ToLower()) || x.Name.ToLower().Contains(filter.ToLower()))
                .Skip((pageNumber - 1) * size)
                .Take(size)
                .ToListAsync();
        }

        public async Task<int> GetTotalRecords()
        {
            return await _context.Clientes.CountAsync();
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0 ;
        }

        public async Task Update(Cliente cliente)
        {
             _context.Update(cliente);
            await _context.SaveChangesAsync();
        }
    }
}