using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Entities;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Data.Repository
{
    public class ColaboradorRepository : IColaboradorRepository
    {
        private readonly DataContext _context;


        public ColaboradorRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Colaborador> GetColaboradorByIdAsync(int id)
        {
            return await _context.Colaboradores.FindAsync(id);
        }

        public async Task<Colaborador> GetColaboradorByNameAsync(string name)
        {
            
            return await  _context.Colaboradores.SingleOrDefaultAsync(x => x.UserName == name);
        }

        public async Task<IEnumerable<Colaborador>> GetColaboradoresAsync(int pageNumber, int size, string cliente)
        {
                        
            return await  _context.Colaboradores
                .Where(x => x.UserName.ToLower().Contains(cliente.ToLower()))
                .Skip((pageNumber - 1) * size)
                .Take(size)
                .ToListAsync();
        }
        public async Task<int> GetTotalRecords()
        {
            return await _context.Colaboradores.CountAsync();
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0 ;
        }

        public void Update(Colaborador colaborador)
        {
            _context.Entry(colaborador).State = EntityState.Modified;
        }

    }
}