﻿using AutoMapper;
using backend.DTOs;
using backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Helpers
{
    public class AutoMapperProfiles : Profile
    {

        public AutoMapperProfiles()
        {
            CreateMap<HistoricoItem, HistoricoItemDto>()
                .ForMember(dest => dest.ColaboradorName,
                    opts => opts.MapFrom(src => src.Colaborador.UserName))
                .ForMember(dest => dest.ClienteName,
                    opts => opts.MapFrom(src => src.Cliente.UserName))
                .ForMember(dest => dest.ClienteId,
                    opts => opts.MapFrom(src => src.Cliente.Id))
                .ForMember(dest => dest.ColaboradorId,
                    opts => opts.MapFrom(src => src.Colaborador.Id)).ReverseMap();

            CreateMap<Cliente, ClienteDto>().ReverseMap();

        }
    }
}
