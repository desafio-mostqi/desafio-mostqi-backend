﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    public class Cotacao
    {
        public Cotacao(string valor, string high, string low, DateTime data)
        {
            Valor = valor;
            High = high;
            Low = low;
            Data = data;
        }

        public int Id { get; set; }

        public string Valor { set; get; }

        public string High { get; set; }

        public string Low { get; set; }

        public DateTime Data { get; set; }
    }
}
