﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Entities
{
    public class HistoricoItem

    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }

        public Colaborador Colaborador { get; set; }

        public string Acao { get; set; }

        public DateTime Data { get; set; }

        

        public HistoricoItem(int id, Cliente cliente, Colaborador colaborador, string acao, DateTime data)
        {
            Id = id;
            Cliente = cliente;
            Colaborador = colaborador;
            Acao = acao;
            Data = data;
        }

        public HistoricoItem()
        {
        }
    }
}
