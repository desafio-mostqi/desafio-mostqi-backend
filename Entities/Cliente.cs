namespace backend.Entities
{
    public class Cliente
    {

        public int Id { get; set; }

        public string UserName { get; set; }

        public string Name { get; set; }

        public string Rg { get; set; }

        public string DataNascimento { get; set; }

        public string ImageBase64 { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

    }
}