using System;

namespace backend.DTOs
{
    public class PagedResponse<T>
    {

        public T Data { get; set; }
        public bool Succeeded { get; set; }
        public string[] Errors { get; set; }
        public string Message { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Uri FirstPage { get; set; }
        public Uri LastPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public Uri NextPage { get; set; }
        public Uri PreviousPage { get; set; }

        
        public PagedResponse(T data, int pageNumber, int pageSize, int totalRecords)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Data = data;
            this.TotalRecords = totalRecords;

            var q =  totalRecords/pageSize;

            this.TotalPages = totalRecords%pageSize == 0 ? q : q + 1;
            
            this.Message = null;
            this.Succeeded = true;
            this.Errors = null;
        }
    }
}