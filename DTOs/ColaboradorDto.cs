namespace backend.DTOs
{
    public class ColaboradorDto : IUserDto
    {

        public int Id { get; set; }
        public string Username { get; set; }

        public string Password;
        public string Token { get; set; }
        public bool isColaborador { get; set; } = true;
    }
}