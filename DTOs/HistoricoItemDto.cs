﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class HistoricoItemDto
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }

        public int ColaboradorId { get; set; }

        public string ClienteName { get; set; }

        public string ColaboradorName { get; set; }

        public string Acao { get; set; }

        public DateTime Data { get; set; }

        

    }
}
