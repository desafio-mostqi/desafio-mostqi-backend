﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class CotacaoFromAPI
    {

        [JsonProperty("USD/BRL")]
        public UsdBrl UsdBrl { get; set; }

    }
   

    public partial class UsdBrl
    {
        
       
        public Value[] Values { get; set; }

       
        public string Status { get; set; }
    }

   

    public partial class Value
    {
       
        public DateTime Datetime { get; set; }

       
        public string Open { get; set; }

        
        public string High { get; set; }

       
        public string Low { get; set; }

        
        public string Close { get; set; }
    }
}
