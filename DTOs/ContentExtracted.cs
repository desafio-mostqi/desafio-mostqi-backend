using System;
using System.Collections.Generic;
using System.Globalization;

namespace backend.DTOs
{


    public partial class ContentExtracted
    {

        public Result[]? Result { get; set; }

        public string? RequestId { get; set; }

        public long? ElapsedMilliseconds { get; set; }

        public Status? Status { get; set; }
    }

    public partial class Result
    {
        public Field[]? Fields { get; set; }

        public object? Image { get; set; }

        public double? Score { get; set; }

        public string? Type { get; set; }

        public string? StdType { get; set; }

        public long? PageNumber { get; set; }

        public string[]? Tags { get; set; }

        public object[]? Tables { get; set; }

        public string? Feature { get; set; }

        public Vertice[]? Vertices { get; set; }

        public double? Area { get; set; }

        public string? ProcessType { get; set; }
    }

    public partial class Field
    {
        public string? Name { get; set; }

        public object? StdName { get; set; }

        public string? Value { get; set; }

        public double? Score { get; set; }
    }

    public partial class Vertice
    {
        public string? Name { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

    }

    public partial class Status
    {
        public string? Message { get; set; }


        public long? Code { get; set; }


        public object? Errors { get; set; }
    }
}