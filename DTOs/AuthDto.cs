namespace backend.DTOs
{
    public class AuthDto
    {
        public string Token { get ; set; }

        public AuthDto(string token)
        {
            Token = token;
        }
    }
}