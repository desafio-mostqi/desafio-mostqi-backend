namespace backend.DTOs
{
    public class ClienteDto : IUserDto
    {

        public int CurrentUser { get; set; }
        public int Id { get; set; }

        public string Username { get; set; }

        public string Token { get; set; }

        public string Name { get; set; }

        public string ImageBase64 { get; set; }

        public string Rg { get; set; }

        public string Password { get; set; }

        public string DataNascimento { get; set; }

        public bool isColaborador { get; set; } = false;
    }
}