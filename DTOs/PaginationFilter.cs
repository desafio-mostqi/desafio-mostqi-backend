using System;

namespace backend.Helpers
{
    public class PaginationFilter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public DateTime? DataInicio { get; set; }

        public DateTime? DataFim { get; set; }

        public string ColabFilter { get; set; }
        public string ClientFilter { get; set; }
        public PaginationFilter()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
            this.ColabFilter = "";
            this.ClientFilter = "";
        }
        public PaginationFilter(string colabFilter, string clientFilter, int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
            this.ColabFilter = colabFilter != null && colabFilter.Length > 0 ? colabFilter : "";
            this.ClientFilter = clientFilter != null && clientFilter.Length > 0 ? clientFilter : "";
        }

        public PaginationFilter(string clientFilter, int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
            this.ClientFilter = clientFilter != null && clientFilter.Length > 0 ? clientFilter : "";
        }

        public PaginationFilter( int pageNumber, int pageSize, DateTime? filterInicio, DateTime? filterFim)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
            if(filterInicio != null && filterFim != null)
            {
                this.DataInicio = filterInicio;
                this.DataFim = filterFim;
            }
            
        }
    }
}