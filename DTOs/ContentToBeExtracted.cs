namespace backend.DTOs
{
    public class ContentToBeExtracted
    {

        public string FileBase64 { get; set; }

        public string FileUrl { get; set; }

        public bool ReturnImage { get; set; } = false;
        public string Type { get; set; }

        public string Tag { get; set; }

        public ContentToBeExtracted(string fileBase64, string fileUrl, bool returnImage, string type, string tag)
        {
            FileBase64 = fileBase64;
            FileUrl = fileUrl;
            ReturnImage = returnImage;
            Type = type;
            Tag = tag;
        }
        

    }
}